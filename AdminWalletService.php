<?php

namespace App\Http\Services;

class AdminWalletService{
    // ADMIN
    // 1. Deposit user Wallet
    // 2. Withdraw/Deduct user wallaet
    // 3. Lock Wallet (Restrict User from using their wallet for transfers, requesting for withdrawal, and funding wallet)
    // A user's wallet can be locked for emergencies such as account compromise, threat, etc
    // 4. Wallet Funding History
    // 5. Wallet Limit (This will put a limit/maximum on how much the user can fund their USD,NGN, GBP wallet with)
    // 6. Wallet Config (user can create wallet based on the ones activated by admin)

    public function depositWallet(){

    }

    public function deductWallet(){

    }

    public function lockWallet(){

    }

    public function walletFundingHistory(){

    }

    public function limitWallet(){

    }

    public function createWallet(){

    }

    public function updateWallet(){

    }

    public function deleteWallet(){

    }
}